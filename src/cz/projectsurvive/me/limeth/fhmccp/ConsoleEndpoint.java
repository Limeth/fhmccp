package cz.projectsurvive.me.limeth.fhmccp;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.regex.Pattern;

import javafx.application.Platform;
import javafx.scene.control.TextArea;

import javax.websocket.ClientEndpoint;
import javax.websocket.CloseReason;
import javax.websocket.ContainerProvider;
import javax.websocket.DeploymentException;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;

import org.apache.commons.lang3.StringEscapeUtils;

import cz.projectsurvive.me.limeth.fhmccp.exceptions.FHLoggedOutException;

@ClientEndpoint
public class ConsoleEndpoint
{
	private static final String[] COLOR_CODES = {
		"[0;30;22m", "[0;34;22m", "[0;32;22m", "[0;36;22m",
		"[0;31;22m", "[0;35;22m", "[0;33;22m", "[0;37;22m",
		"[0;30;1m", "[0;34;1m", "[0;32;1m", "[0;36;1m",
		"[0;31;1m", "[0;35;1m", "[0;33;1m", "[0;37;1m",
		"[21m", "[9m", "[3m", "[4m", "[m", "[5m"
	};
	
	private FHService service;
	
	private ConsoleEndpoint(FHService service)
	{
		this.service = service;
	}
	
	public static Session connect(FHService service) throws DeploymentException, IOException, URISyntaxException, FHLoggedOutException
	{
		String address = service.getWebSocketAddress();
		ConsoleEndpoint endpoint = new ConsoleEndpoint(service);
		URI uri = new URI(address);
		
		System.out.println("Connecting to " + address + "...");
		
		Session session = ContainerProvider.getWebSocketContainer().connectToServer(endpoint, uri);
		
		System.out.println("Connected.");
		
		return session;
	}
	
	@OnOpen
	public void onOpen(Session session, EndpointConfig config)
	{
		try
		{
			session.getBasicRemote().sendText("console_login " + service.getConsoleKey());
		}
		catch(IOException | FHLoggedOutException e)
		{
			e.printStackTrace();
		}
	}
	
	@OnMessage
	public void onMessage(String message)
	{
		message = StringEscapeUtils.unescapeHtml4(message);
		message = removeColorCodes(message);
		
		if(message.startsWith("CPU USAGE: "))
		{
			int endIndex = message.indexOf('%') - 1;
			String rawPercentage = message.substring("CPU USAGE: ".length(), endIndex);
			int percentage = Integer.parseInt(rawPercentage);
			
			addPerformanceEntry(percentage);
			return;
		}
		
		if(message.startsWith(">"))
			message = message.substring(1);
		
		if(message.startsWith("\n"))
			message = message.substring(1);
		
		append(message + '\n');
	}
	
	private String removeColorCodes(String message)
	{
		for(String colorCode : COLOR_CODES)
			message = message.replaceAll(Pattern.quote(colorCode), "");
		
		return message;
	}
	
	@OnClose
	public void onClose(Session session, CloseReason reason)
	{
		System.out.println("Websocket closed: " + reason.getReasonPhrase());
	}
	
	@OnError
	public void onError(Session session, Throwable throwable)
	{
		throwable.printStackTrace();
	}
	
	public FHService getService()
	{
		return service;
	}
	
	public TextArea getConsoleArea()
	{
		return service.getScene().getController().getAreaConsole();
	}
	
	public void append(String text)
	{
		Platform.runLater(new Runnable() {
			@Override
			public void run()
			{
				getConsoleArea().appendText(text);
			}
		});
	}
	
	public void addPerformanceEntry(int percentage)
	{
		Platform.runLater(new Runnable() {
			@Override
			public void run()
			{
				service.getScene().getController().addPerformanceEntry(percentage);
			}
		});
	}
}
