package cz.projectsurvive.me.limeth.fhmccp;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public enum RequestMethod
{
	GET {
		@Override
		public HttpURLConnection openConnection(String host, String file, String arguments) throws IOException
		{
			if(arguments != null)
				arguments = '?' + arguments;
			else
				arguments = "";
			
			HttpURLConnection connection = (HttpURLConnection) new URL("http://" + host + "/" + file + arguments).openConnection();

			connection.setInstanceFollowRedirects(false);
			connection.setUseCaches(false);
			connection.setRequestProperty("User-Agent", "FHMCCP");
			connection.setRequestProperty("Charset", "utf-8");
			
			return connection;
		}
	}, POST {
		@Override
		public HttpURLConnection openConnection(String host, String file, String arguments) throws IOException
		{
			HttpURLConnection connection = (HttpURLConnection) new URL("http://" + host + "/" + file).openConnection();
			
			return connection;
		}
		
		@Override
		public void cookiesSet(HttpURLConnection connection, String host, String file, String arguments) throws IOException
		{
			connection.setInstanceFollowRedirects(false);
			connection.setUseCaches(false);
			connection.setRequestProperty("User-Agent", "FHMCCP");
			connection.setRequestProperty("Charset", "utf-8");
			connection.setRequestMethod("POST");
			connection.setDoOutput(true);
			
			DataOutputStream output = new DataOutputStream(connection.getOutputStream());
			
			output.writeBytes(arguments);
			output.flush();
		}
	};
	
	public abstract HttpURLConnection openConnection(String host, String file, String arguments) throws IOException;
	public void cookiesSet(HttpURLConnection connnection, String host, String file, String arguments) throws IOException {}
}
