package cz.projectsurvive.me.limeth.fhmccp;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.hccp.net.CookieManager;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import cz.projectsurvive.me.limeth.fhmccp.exceptions.FHLoggedOutException;
import cz.projectsurvive.me.limeth.fhmccp.exceptions.FHLoginException;

public class FakaHeda
{
	private static final Pattern WEBSOCKET_PORT_PATTERN = Pattern.compile("new Socket\\(\"ws://.{2}\\..{2}\\..{3}\\..{3}:(.{5})/\"\\);");
	private static CookieManager cookieManager = new CookieManager();
	private static FHService[] lazyServices;
	
	public static HttpURLConnection openConnection(String host, String file, String arguments, RequestMethod method) throws IOException
	{
		HttpURLConnection connection = method.openConnection(host, file, arguments);
		
		cookieManager.setCookies(connection);
		method.cookiesSet(connection, host, file, arguments);
		cookieManager.storeCookies(connection);
		
		return connection;
	}
	
	public static HttpURLConnection openConnection(String file, String arguments, RequestMethod method) throws IOException
	{
		return openConnection("www.fakaheda.eu", file, arguments, method);
	}
	
	public static void login(String username, String password) throws IOException, FHLoginException
	{
		lazyServices = null;
		username = URLEncoder.encode(username, "utf-8");
		password = URLEncoder.encode(password, "utf-8");
		String args = "login=" + username + "&password=" + password;
		HttpURLConnection connection = openConnection("prihlasit", args, RequestMethod.POST);
		String location = connection.getHeaderField("Location");
		
		if(location != null && location.endsWith("://www.fakaheda.eu/"))
			return;
		
		Document document = Jsoup.parse(connection.getInputStream(), "utf-8", connection.getURL().toString());
		Element errorElement = document.getElementsByClass("error").get(0);
		String error = errorElement.ownText();
		
		throw new FHLoginException(error);
	}
	
	public static FHService[] fetchServices(Predicate<FHService> predicate) throws IOException, FHLoggedOutException
	{
		URLConnection connection = openConnection("control_panel/search_servers.js", null, RequestMethod.GET);
		
		checkLogin(connection);
		
		InputStream is = connection.getInputStream();
		InputStreamReader reader = new InputStreamReader(is);
		JsonParser parser = new JsonParser();
		JsonElement root = parser.parse(reader);
		lazyServices = FHService.fromJson(root);
		List<FHService> list = Arrays.stream(lazyServices).filter(predicate).collect(Collectors.toList());
		
		return list.toArray(new FHService[list.size()]);
	}
	
	public static FHService[] fetchServices() throws IOException, FHLoggedOutException
	{
		URLConnection connection = openConnection("control_panel/search_servers.js", null, RequestMethod.GET);
		
		checkLogin(connection);
		
		InputStream is = connection.getInputStream();
		InputStreamReader reader = new InputStreamReader(is);
		JsonParser parser = new JsonParser();
		JsonElement root = parser.parse(reader);
		
		return lazyServices = FHService.fromJson(root);
	}
	
	public static FHService[] getServices() throws IOException, FHLoggedOutException
	{
		return lazyServices != null ? lazyServices : fetchServices();
	}
	
	public static FHService[] getServices(Predicate<FHService> predicate) throws IOException, FHLoggedOutException
	{
		if(lazyServices == null)
			return fetchServices(predicate);
		
		List<FHService> list = Arrays.stream(lazyServices).filter(predicate).collect(Collectors.toList());
		
		return list.toArray(new FHService[list.size()]);
	}
	
	public static final Predicate<FHService> IS_MINECRAFT_SERVICE = service ->
	{
		String source = service.getImageSource();
		String[] units = source.split("/");
		String lastUnit = units[units.length - 1];
		
		return lastUnit.startsWith("mncb");
	};
	
	public static int fetchWebSocketPort(int id) throws IOException, FHLoggedOutException
	{
		URLConnection connection = openConnection("control_panel/game_servers/" + id + "/console", null, RequestMethod.GET);
		
		checkLogin(connection);
		
		Scanner scanner = new Scanner(connection.getInputStream());
		String page = "";
		
		while(scanner.hasNextLine())
			page += scanner.nextLine() + '\n';
		
		scanner.close();
		
		Matcher matcher = WEBSOCKET_PORT_PATTERN.matcher(page);
		
		if(matcher.find())
			return Integer.parseInt(matcher.group(1));
		
		throw new IOException("WS Port not found.");
	}
	
	public static String fetchConsoleKey(int id) throws IOException, FHLoggedOutException
	{
		URLConnection connection = openConnection("control_panel/game_servers/" + id + "/get_console_key", null, RequestMethod.GET);
		
		checkLogin(connection);
		
		JsonParser parser = new JsonParser();
		InputStream is = connection.getInputStream();
		InputStreamReader reader = new InputStreamReader(is);
		JsonObject root = parser.parse(reader).getAsJsonObject();
		
		reader.close();
		
		return root.get("result").getAsString();
	}
	
	public static void executeServerTask(int id, ServerTask task) throws IOException, FHLoggedOutException
	{
		URLConnection connection = openConnection("control_panel/servers/" + id + "/" + task.getId(), null, RequestMethod.GET);
		
		checkLogin(connection);
	}
	
	public static void checkLogin(URLConnection connection) throws FHLoggedOutException
	{
		String location = connection.getHeaderField("Location");
		
		if(location != null && location.startsWith("http://www.fakaheda.eu/prihlasit"))
			throw new FHLoggedOutException();
	}
	
	public static CookieManager getCookieManager()
	{
		return cookieManager;
	}
}
