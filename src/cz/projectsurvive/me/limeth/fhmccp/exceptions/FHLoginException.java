package cz.projectsurvive.me.limeth.fhmccp.exceptions;

public class FHLoginException extends IllegalArgumentException
{
	private static final long serialVersionUID = -8018007961882103609L;
	
	public FHLoginException(String message)
	{
		super(message);
	}
}
