package cz.projectsurvive.me.limeth.fhmccp;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import javafx.application.Application;
import javafx.stage.Stage;

import javax.websocket.Session;

import cz.projectsurvive.me.limeth.fhmccp.exceptions.FHLoggedOutException;
import cz.projectsurvive.me.limeth.fhmccp.exceptions.FHLoginException;
import cz.projectsurvive.me.limeth.fhmccp.fx.LoginScene;


public final class FHMCCP extends Application
{
	@SuppressWarnings("resource")
	public static void mainCLI(String[] args) throws InterruptedException
	{
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Přihlašovací jméno: ");
		
		String username = scanner.nextLine();
		
		System.out.print("Heslo uživatele " + username + ": ");
		
		String password = scanner.nextLine();
		
		try
		{
			FakaHeda.login(username, password);
		}
		catch(FHLoginException e)
		{
			System.out.println(e.getMessage());
			System.exit(0);
		}
		catch(IOException e)
		{
			e.printStackTrace();
			System.exit(0);
		}
		
		FHService[] services = null;
		
		try
		{
			services = FakaHeda.fetchServices(FakaHeda.IS_MINECRAFT_SERVICE);
		}
		catch(IOException e)
		{
			e.printStackTrace();
			System.exit(0);
		}
		catch(FHLoggedOutException e)
		{
			System.out.println("Byl(a) jste odhlášen(a).");
			System.exit(0);
		}
		
		if(services.length <= 0)
		{
			System.out.println("Nemáte předplacené žádné Minecraft servery.");
			System.exit(0);
		}
		
		FHService selectedService = null;
		
		do
		{
			for(int i = 0; i < services.length; i++)
				System.out.println("\t[" + (i + 1) + "] " + services[i].getName());
			
			System.out.print("Zadejte číslo serveru: ");
			
			int serviceIndex;
			
			try
			{
				serviceIndex = Integer.parseInt(scanner.nextLine()) - 1;
			}
			catch(NumberFormatException e)
			{
				System.out.println("Neplatné číslo serveru.");
				continue;
			}
			
			if(serviceIndex < 0 || serviceIndex >= services.length)
			{
				System.out.println("Neplatné číslo serveru.");
				continue;
			}
			
			selectedService = services[serviceIndex];
		}
		while(selectedService == null);
		
		Session session = null;
		
		try
		{
			session = selectedService.connectConsole();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.exit(0);
		}
		
		do
		{
			String line = scanner.nextLine();
			
			session.getAsyncRemote().sendText(line);
		}
		while(true);
	}
	
	public static void main(String[] args) throws InterruptedException
	{
		List<String> argList = Arrays.asList(args);
		
		if(argList.contains("--cli"))
			mainCLI(args);
		else
			Application.launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception
	{
        primaryStage.setScene(new LoginScene());
		primaryStage.setTitle("FHMCCP - Přihlášení");
        primaryStage.show();
	}
	
	public static String getGlobalStylesheet()
	{
		return FHMCCP.class.getResource("fx/template/global.css").toExternalForm();
	}
	
	public static <T> T debug(T message)
	{
		System.out.println("[DEBUG] " + message);
		
		return message;
	}
}
