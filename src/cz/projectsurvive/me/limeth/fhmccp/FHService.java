package cz.projectsurvive.me.limeth.fhmccp;

import java.io.IOException;

import javax.websocket.Session;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import cz.projectsurvive.me.limeth.fhmccp.exceptions.FHLoggedOutException;
import cz.projectsurvive.me.limeth.fhmccp.fx.ServerScene;

public class FHService
{
	public static final int PORT_OFFSET_WEBSOCKET = -2000;
	private static final String ADDRESS_DELIMITER = ", ";
	private String link, imageSource, label, lazyName, lazyAddress, lazyHost, lazyConsoleKey;
	private int id;
	private Integer lazyServerPort, lazyWebSocketPort;
	private ServerScene scene;
	
	public FHService(String link, String imageSource, String label, int id)
	{
		this.link = link;
		this.imageSource = imageSource;
		this.label = label;
		this.id = id;
	}
	
	public static FHService fromJson(JsonObject object)
	{
		String link = object.get("link").getAsString();
		String imageSource = object.get("imgsrc").getAsString();
		String label = object.get("label").getAsString();
		int id = object.get("value").getAsInt();
		
		return new FHService(link, imageSource, label, id);
	}
	
	public static FHService[] fromJson(JsonArray array)
	{
		FHService[] result = new FHService[array.size()];
		
		for(int i = 0; i < result.length; i++)
			result[i] = fromJson(array.get(i).getAsJsonObject());
		
		return result;
	}
	
	public static FHService[] fromJson(JsonElement element)
	{
		if(element instanceof JsonObject)
			return new FHService[] { fromJson((JsonObject) element) };
		else if(element instanceof JsonArray)
			return fromJson((JsonArray) element);
		else
			throw new UnsupportedOperationException();
	}
	
	public String initName()
	{
		String[] data = label.split(", ");
		lazyName = data[0];
		
		for(int i = 1; i < data.length - 1; i++)
			lazyName += ", " + data[i];
		
		return lazyName;
	}
	
	public String getName()
	{
		return lazyName != null ? lazyName : initName();
	}
	
	private String initAddress()
	{
		String[] split = label.split(ADDRESS_DELIMITER);
		lazyAddress = split[split.length - 1];
		
		return lazyAddress;
	}
	
	public String getAddress()
	{
		return lazyAddress != null ? lazyAddress : initAddress();
	}
	
	private String initHost()
	{
		String address = getAddress();
		
		return lazyHost = address.split(":", 2)[0];
	}
	
	public String getHost()
	{
		return lazyHost != null ? lazyHost : initHost();
	}
	
	private int initPort()
	{
		String address = getAddress();
		String rawPort = address.split(":", 2)[1];
		
		return lazyServerPort = Integer.parseInt(rawPort);
	}
	
	public int getServerPort()
	{
		return lazyServerPort != null ? lazyServerPort : initPort();
	}
	
	public String getLink()
	{
		return link;
	}
	
	public String getWebSocketAddress() throws IOException, FHLoggedOutException
	{
		return "ws://" + getHost() + ":" + getWebSocketPort() + "/";
	}
	
	public void executeTask(ServerTask task) throws IOException, FHLoggedOutException
	{
		FakaHeda.executeServerTask(id, task);
	}
	
	public int fetchWebSocketPort() throws IOException, FHLoggedOutException
	{
		return lazyWebSocketPort = FakaHeda.fetchWebSocketPort(id);
	}
	
	public int getWebSocketPort() throws IOException, FHLoggedOutException
	{
		return lazyWebSocketPort != null ? lazyWebSocketPort : fetchWebSocketPort();
	}
	
	public String fetchConsoleKey() throws IOException, FHLoggedOutException
	{
		return lazyConsoleKey = FakaHeda.fetchConsoleKey(id);
	}
	
	public String getConsoleKey() throws IOException, FHLoggedOutException
	{
		return lazyConsoleKey != null ? lazyConsoleKey : fetchConsoleKey();
	}
	
	public Session connectConsole() throws IOException, FHLoggedOutException, Exception
	{
		return ConsoleEndpoint.connect(this);
	}
	
	public void setLink(String link)
	{
		this.link = link;
	}

	public String getImageSource()
	{
		return imageSource;
	}

	public void setImageSource(String imageSource)
	{
		this.imageSource = imageSource;
	}

	public String getLabel()
	{
		return label;
	}

	public void setLabel(String label)
	{
		this.label = label;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}
	
	@Override
	public String toString()
	{
		return "FHService [link=" + link + ", imageSource=" + imageSource + ", label=" + label + ", id=" + id + "]";
	}
	
	public boolean hasScene()
	{
		return scene != null;
	}

	public ServerScene getScene()
	{
		return scene;
	}

	public void setScene(ServerScene scene)
	{
		this.scene = scene;
	}
}
