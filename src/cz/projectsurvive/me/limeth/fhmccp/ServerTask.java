package cz.projectsurvive.me.limeth.fhmccp;

public enum ServerTask
{
	START("start"), RESTART("restart"), PAUSE("stop");
	
	private final String id;
	
	private ServerTask(String id)
	{
		this.id = id;
	}
	
	public String getId()
	{
		return id;
	}
}
