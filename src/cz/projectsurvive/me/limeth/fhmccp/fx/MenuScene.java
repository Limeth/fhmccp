package cz.projectsurvive.me.limeth.fhmccp.fx;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import javax.websocket.DeploymentException;

import cz.projectsurvive.me.limeth.fhmccp.FHMCCP;
import cz.projectsurvive.me.limeth.fhmccp.FHService;
import cz.projectsurvive.me.limeth.fhmccp.FakaHeda;
import cz.projectsurvive.me.limeth.fhmccp.exceptions.FHLoggedOutException;

public class MenuScene extends Scene implements EventHandler<WindowEvent>
{
	private static MenuScene instance;
	private final ArrayList<Button> buttons = new ArrayList<Button>();
	
	public MenuScene() throws IOException
	{
		super(FXMLLoader.load(FHMCCP.class.getResource("fx/template/MenuTemplate.fxml")));
		getStylesheets().add(FHMCCP.getGlobalStylesheet());
		updateButtons();
	}
	
	private static Stage inNewStage() throws IOException
	{
		instance = new MenuScene();
		Stage stage = new Stage();
		
		stage.setOnCloseRequest(instance);
		stage.setScene(instance);
		stage.setTitle("FHMCCP - Výběr serveru");
		stage.show();
		
		return stage;
	}
	
	public static Stage show() throws IOException
	{
		if(instance == null)
			return inNewStage();
		
		Stage stage = (Stage) instance.getWindow();
		
		instance.updateButtons();
		stage.requestFocus();
		
		return stage;
	}
	
	public void updateButtons() throws IOException
	{
		VBox root = getRootVBox();
		ObservableList<Node> children = root.getChildren();
		FHService[] services;
		
		children.clear();
		buttons.clear();
		
		try
		{
			services = FakaHeda.getServices(FakaHeda.IS_MINECRAFT_SERVICE);
		}
		catch(FHLoggedOutException e)
		{
			throw new IOException(e); //TODO ask for relog
		}
		
		for(FHService service : services)
		{
			String name = service.getName();
			Button button = new Button(name);
			ServerButtonHandler handler = new ServerButtonHandler(service);
			
			button.setOnAction(handler);
			button.setDisable(service.hasScene());
			children.add(button);
			buttons.add(button);
		}
	}
	
	public VBox getRootVBox()
	{
		return (VBox) getRoot();
	}
	
	private class ServerButtonHandler implements EventHandler<ActionEvent>
	{
		private final FHService service;
		
		public ServerButtonHandler(FHService service)
		{
			this.service = service;
		}
		
		@Override
		public void handle(ActionEvent event)
		{
			try
			{
				instance = null;
				ServerScene.inNewStage(service);
				((Stage) getWindow()).close();
			}
			catch(IOException | DeploymentException | URISyntaxException e)
			{
				e.printStackTrace();
			}
			catch(FHLoggedOutException e)
			{
				//TODO ask for relog
			}
		}
	}

	@Override
	public void handle(WindowEvent event)
	{
		instance = null;
	}
}
