package cz.projectsurvive.me.limeth.fhmccp.fx;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import com.sun.javafx.stage.StageHelper;

import cz.projectsurvive.me.limeth.fhmccp.FHService;
import cz.projectsurvive.me.limeth.fhmccp.ServerTask;
import cz.projectsurvive.me.limeth.fhmccp.exceptions.FHLoggedOutException;

public class ServerController
{
	public static final int HISTORY_COMMAND_MAX_SIZE = 100, HISTORY_CHART_PERFORMANCE_MAX_SIZE = 8;
	
	@FXML private VBox serverRoot;
	@FXML private Button buttonLogout;
	@FXML private Button buttonNew;
	@FXML private Label labelServerName;
	@FXML private Label labelServerAddress;
	@FXML private Button buttonServerStart;
	@FXML private Button buttonServerRestart;
	@FXML private Button buttonServerPause;
	@FXML private TextArea areaConsole;
	@FXML private TextField fieldCommand;
	@FXML private Button buttonCommand;
	@FXML private AreaChart<Long, Integer> chartPerformance;
	private Series<Long, Integer> chartPerformanceCPU;
	private FHService service;
	private ServerScene scene;
	private final ArrayList<String> commandHistory = new ArrayList<String>();
	private Integer commandHistoryIndex;
	private long initializedAt;
	
	public void initialize(FHService service, ServerScene scene)
	{
		this.service = service;
		this.scene = scene;
		String serviceName = service.getName();
		String serviceAddress = service.getAddress();
		ObservableList<Series<Long, Integer>> chartPerformanceData = chartPerformance.getData();
		
	//	chartPerformance.getXAxis().setTickLabelsVisible(false);
		chartPerformanceCPU = new Series<Long, Integer>();
		chartPerformanceCPU.setName("Vytížení procesoru");
		chartPerformanceData.add(chartPerformanceCPU);
		labelServerName.setText(serviceName);
		labelServerAddress.setText(serviceAddress);
		buttonLogout.setOnAction(HANDLER_BUTTON_LOGOUT);
		buttonNew.setOnAction(HANDLER_BUTTON_NEW);
		buttonServerStart.setOnAction(HANDLER_BUTTON_SERVER_START);
		buttonServerRestart.setOnAction(HANDLER_BUTTON_SERVER_RESTART);
		buttonServerPause.setOnAction(HANDLER_BUTTON_SERVER_PAUSE);
		fieldCommand.setOnKeyPressed(HANDLER_KEY_COMMAND);
		fieldCommand.setOnAction(HANDLER_BUTTON_COMMAND);
		buttonCommand.setOnAction(HANDLER_BUTTON_COMMAND);
		initializedAt = System.currentTimeMillis();
	}

	public void addPerformanceEntry(int percentage)
	{
		ObservableList<Data<Long, Integer>> data = chartPerformanceCPU.getData();
//		long now = System.currentTimeMillis();
//		long difference = now - initializedAt;
		
		data.add(new Data<Long, Integer>((long) data.size(), percentage));
		
		while(data.size() > HISTORY_CHART_PERFORMANCE_MAX_SIZE)
		{
			data.remove(0);
			
			for(Data<Long, Integer> entry : data)
				entry.setXValue(entry.getXValue() - 1);
		}
	}
	
	public ServerScene getScene()
	{
		return scene;
	}
	
	public FHService getService()
	{
		return service;
	}
	
	private final EventHandler<ActionEvent> HANDLER_BUTTON_LOGOUT = new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event)
		{
			Stage loginStage;
			
			try
			{
				loginStage = LoginScene.inNewStage(null);
			}
			catch(IOException e)
			{
				e.printStackTrace();
				return;
			}
			
			HashSet<Stage> stages = new HashSet<Stage>();
			
			for(Stage stage : StageHelper.getStages())
				if(loginStage != stage)
					stages.add(stage);
			
			for(Stage stage : stages)
				stage.close();
		}
	};
	
	private final EventHandler<ActionEvent> HANDLER_BUTTON_NEW = new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event)
		{
			try
			{
				MenuScene.show();
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
		}
	};
	
	private final EventHandler<ActionEvent> HANDLER_BUTTON_SERVER_START = new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event)
		{
			try
			{
				service.executeTask(ServerTask.START);
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
			catch(FHLoggedOutException e)
			{
				e.printStackTrace();
			}
		}
	};
	
	private final EventHandler<ActionEvent> HANDLER_BUTTON_SERVER_RESTART = new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event)
		{
			try
			{
				service.executeTask(ServerTask.RESTART);
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
			catch(FHLoggedOutException e)
			{
				e.printStackTrace();
			}
		}
	};
	
	private final EventHandler<ActionEvent> HANDLER_BUTTON_SERVER_PAUSE = new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event)
		{
			try
			{
				service.executeTask(ServerTask.PAUSE);
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
			catch(FHLoggedOutException e)
			{
				e.printStackTrace();
			}
		}
	};
	
	private final EventHandler<ActionEvent> HANDLER_BUTTON_COMMAND = new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event)
		{
			String command = fieldCommand.getText();
			commandHistoryIndex = null;
			
			getScene().getConsoleSession().getAsyncRemote().sendText(command);
			addCommandToHistory(command);
			fieldCommand.setText(null);
		}
	};
	
	private final EventHandler<KeyEvent> HANDLER_KEY_COMMAND = new EventHandler<KeyEvent>() {
		@Override
		public void handle(KeyEvent event)
		{
			KeyCode key = event.getCode();
			Integer index;
			
			if(key == KeyCode.UP)
				index = increaseCommandHistoryIndex(1);
			else if(key == KeyCode.DOWN)
				index = increaseCommandHistoryIndex(-1);
			else
				return;
			
			String command = index == null ? null : commandHistory.get(index);
			
			fieldCommand.setText(command);
		}
	};
	
	private Integer increaseCommandHistoryIndex(int by)
	{
		Integer index = commandHistoryIndex == null ? -1 : commandHistoryIndex;
		index += by;
		
		if(index < 0 || commandHistory.size() <= 0)
			index = null;
		else if(index >= commandHistory.size())
			index = commandHistory.size() - 1;
		
		return commandHistoryIndex = index;
	}
	
	public void addCommandToHistory(String command)
	{
		commandHistory.add(0, command);
		
		while(commandHistory.size() > HISTORY_COMMAND_MAX_SIZE)
			commandHistory.remove(commandHistory.size() - 1);
	}

	public Button getButtonLogout()
	{
		return buttonLogout;
	}

	public Button getButtonNew()
	{
		return buttonNew;
	}

	public Label getLabelServerName()
	{
		return labelServerName;
	}

	public Label getLabelServerAddress()
	{
		return labelServerAddress;
	}

	public Button getButtonServerStart()
	{
		return buttonServerStart;
	}

	public Button getButtonServerRestart()
	{
		return buttonServerRestart;
	}

	public Button getButtonServerPause()
	{
		return buttonServerPause;
	}

	public TextArea getAreaConsole()
	{
		return areaConsole;
	}

	public TextField getFieldCommand()
	{
		return fieldCommand;
	}

	public Button getButtonCommand()
	{
		return buttonCommand;
	}

	public AreaChart<Long, Integer> getChartPerformance()
	{
		return chartPerformance;
	}
}
