package cz.projectsurvive.me.limeth.fhmccp.fx;

import java.io.IOException;
import java.net.URISyntaxException;

import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import javax.websocket.DeploymentException;
import javax.websocket.Session;

import cz.projectsurvive.me.limeth.fhmccp.ConsoleEndpoint;
import cz.projectsurvive.me.limeth.fhmccp.FHMCCP;
import cz.projectsurvive.me.limeth.fhmccp.FHService;
import cz.projectsurvive.me.limeth.fhmccp.exceptions.FHLoggedOutException;

public class ServerScene extends Scene implements EventHandler<WindowEvent>
{
	private final FHService service;
	private ServerController controller;
	private Session consoleSession;
	
	private ServerScene(VBox parent, FHService service) throws IOException
	{
		super(parent);
		
		this.service = service;
		
		getStylesheets().add(FHMCCP.getGlobalStylesheet());
		service.setScene(this);
	}
	
	private static ServerScene construct(FHService service) throws IOException, DeploymentException, URISyntaxException, FHLoggedOutException
	{
		FXMLLoader loader = new FXMLLoader(FHMCCP.class.getResource("fx/template/ServerTemplate.fxml"));
		VBox parent = (VBox) loader.load();
		ServerScene scene = new ServerScene(parent, service);
		scene.controller = loader.getController();
		
		scene.controller.initialize(service, scene);
		scene.consoleSession = ConsoleEndpoint.connect(service);
		
		return scene;
	}
	
	public static Stage inNewStage(FHService service) throws IOException, DeploymentException, URISyntaxException, FHLoggedOutException
	{
		ServerScene scene = construct(service);
		
		Stage stage = new Stage();
		
		stage.setScene(scene);
		stage.setOnCloseRequest(scene);
		stage.setTitle("FHMCCP - " + service.getName());
		stage.show();
		
		return stage;
	}

	public FHService getService()
	{
		return service;
	}

	@Override
	public void handle(WindowEvent event)
	{
		service.setScene(null);
	}
	
	public ServerController getController()
	{
		return controller;
	}
	
	public Session getConsoleSession()
	{
		return consoleSession;
	}
}
