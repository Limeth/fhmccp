package cz.projectsurvive.me.limeth.fhmccp.fx;

import java.io.IOException;

import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Popup;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;
import cz.projectsurvive.me.limeth.fhmccp.FHMCCP;

public class LoginScene extends Scene
{
	public LoginScene() throws IOException
	{
		super(FXMLLoader.load(FHMCCP.class.getResource("fx/template/LoginTemplate.fxml")));
		getStylesheets().add(FHMCCP.getGlobalStylesheet());
	}
	
	public static Stage inNewStage(Runnable onSuccess) throws IOException
	{
		LoginScene scene = onSuccess != null ? new LoginScene() {
			@Override
			public void onSuccess()
			{
				onSuccess.run();
			}
		} : new LoginScene();
		
		Stage stage = new Stage();
		
		stage.setScene(scene);
		stage.setTitle("FHMCCP - Přihlášení");
		stage.show();
		
		return stage;
	}
	
	public void onSuccess()
	{
		try
		{
			MenuScene.show();
			((Stage) getWindow()).close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public void onFailure(String response)
	{
		showPopup(response, getWindow());
	}
	
	public void showPopup(String response, Window window)
	{
		Popup popup = new Popup();
		ObservableList<Node> content = popup.getContent();
		Label label = new Label(response);
		
		label.getStylesheets().add(FHMCCP.getGlobalStylesheet());
		label.getStyleClass().add("popupLabel");
		content.add(label);
		popup.setAutoFix(true);
	    popup.setAutoHide(true);
	    popup.setHideOnEscape(true);
	    label.setOnMouseReleased(new EventHandler<MouseEvent>() {
	        @Override
	        public void handle(MouseEvent e) {
	            popup.hide();
	        }
	    });
	    popup.setOnShown(new EventHandler<WindowEvent>() {
	        @Override
	        public void handle(WindowEvent e) {
	            popup.setX(window.getX() + window.getWidth()/2 - popup.getWidth()/2);
	            popup.setY(window.getY() + window.getHeight()/2 - popup.getHeight()/2);
	        }
	    });
		popup.show(window);
	}
}
