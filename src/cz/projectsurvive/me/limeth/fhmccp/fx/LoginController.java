package cz.projectsurvive.me.limeth.fhmccp.fx;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import cz.projectsurvive.me.limeth.fhmccp.FakaHeda;
import cz.projectsurvive.me.limeth.fhmccp.exceptions.FHLoginException;

public class LoginController implements Initializable, EventHandler<ActionEvent>
{
	@FXML private TextField fieldUsername;
	@FXML private PasswordField fieldPassword;
	@FXML private Button buttonLogin;
	
	@Override
	public void initialize(URL location, ResourceBundle resources)
	{
		buttonLogin.setOnAction(this);
		fieldUsername.setOnAction(this);
		fieldPassword.setOnAction(this);
	}

	@Override
	public void handle(ActionEvent event)
	{
		String username = fieldUsername.getText();
		String password = fieldPassword.getText();
		LoginScene scene = (LoginScene) buttonLogin.getScene();
		
		try
		{
			FakaHeda.login(username, password);
			scene.onSuccess();
		}
		catch(FHLoginException e)
		{
			scene.onFailure(e.getMessage());
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
}
