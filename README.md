# FakaHeda Minecraft Control Panel

### Ke stažení
* **Nejnovější verze: http://limeth.projectsurvive.cz/fhmccp/latest.php**
* Všechny verze: http://limeth.projectsurvive.cz/fhmccp/

### Využité knihovny
* Apache commons-lang
* Gson
* Jsoup
* Tyrus

### Podmínky úprav a distribuce
This work is licensed under the Creative Commons Attribution-NoDerivatives 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nd/4.0/.